<?php
	require_once("inc/header.php");
?>					
			<div class="row">	
				<div class="col-md-12">
					<h3 class="text-center">
						SERP Auto Checker @ mortarboyz
					</h3>
					<br />
					<br />
				</div>
			</div>
<?php
	if($isUserLogin && !isset($_GET['page'])){
?>
			<div class="row">
				<div class="col-md-2">
				</div>
				<div class="col-md-8">
					<form class="form-inline" method="POST" action="index.php?main">
						<button type="submit" class="btn btn-default" name="submit">Check Rank</button>
					</form>
				</div>
				<div class="col-md-2">
				</div>
			</div>
			<br />
			<div class="row">
				<div class="col-md-2">
				</div>
				<div class="col-md-8 ">
					<table class="table table-bordered table-hover table-condensed">
						<thead>
							<tr>
								<th>No</th>
								<th>URL</th>
								<th>Keyword</th>
								<th>Google</th>
							</tr>
						</thead>
						<tbody>
						<?php
						$userId = $_SESSION['userId'];
						$urutan = 0; //Tentukan nomor nomor
						foreach(db_select_record("keywords", "userid", $userId) as $keywordList){
						if(isset($_POST['submit'])){
							require_once('simple_html_dom.php');
							$q  = "http://www.google.co.id/search?"."q=".str_replace(' ', '%20', $keywordList[1])."&start=0&num=100"; //query pencarian
							$html = file_get_html($q); //Ambil source code
							#main > div:nth-child(27) > div > div:nth-child(1) > a
							$findLink = $html->find('div div div a'); //Cari tag <h3 class="r"><a>; //Cari tag <h3 class="r"><a>
							$serpRank = 0; //Urutan pada SERP 
						?>
						<?php
							foreach ($findLink as $findLinks) {
							$serpRank += 1; //Tambah Urutan SERP
							$searchSerp = trim($findLinks->href); //hapus spasi jika ada
							foreach(db_select_record("websites", "userid", $userId) as $siteLists){
								if(strpos($searchSerp, $siteLists[1])){
									preg_match('/q=(.+)&amp;sa=/U', $searchSerp, $matches);
									$searchSerp = $matches[1]; //Hasil URL yang telah difilter didalam var $matches di masukkan ke dalam var $searchSerp
						?>
							<tr>
								<td><?php echo $urutan += 1; //tambah nomor urut  ?></td>
								<td><?php echo $searchSerp; //tampilkan url serp ?></td>
								<td><?php echo $keywordList[1]; //tampilkan keyword ?></td>
								<td><?php echo $serpRank; //tampilkan urutan serp?></td>
							</tr>
							<?php
								} else {
									continue;
								} 	
							}
						}
					}
				}			?>
						</tbody>
					</table>
				</div>
				<div class="col-md-2">
				</div>
			</div>
<?php
	}else if($isUserLogin && $_GET['page'] == 'config'){
?>
			<div class="row">
				<div class="col-md-2"></div>
				<div class="col-md-4">
					<h4 class="text-center">Keywords</h4>
					<form class="form-inline" method="POST" action="index.php?page=config">
						<div class="form-group">
						    <input type="text" class="form-control" id="keywordAdd" placeholder="add keyword.." name="keywordAdd">
						    <button type="submit" class="btn btn-default" name="submitKeyword">Add Keyword</button>
						</div>
					</form>
					<br />
					<table class="table table-bordered table-hover table-condensed">
						<thead>
							<tr>
								<th>No</th>
								<th>Keyword</th>
								<th>Act</th>
							</tr>
						</thead>
						<tbody>
						<?php
							$userId = $_SESSION['userId'];
							if (isset($_POST['submitKeyword'])) {
								$input = $_POST['keywordAdd'];

								if(user_add("keywords", $input, $userId)){
									echo "<p>Keyword Added</p>";
								}else{
									echo "<p>Keyword Add Error !</p>";
								}
							}else if (isset($_GET['delete'])) {
								if(db_delete_record("keywords", "id", $_GET['delete'])){
									echo "<p>Keyword Deleted</p>";
								}else if(!db_delete_record("keywords", "id", $_GET['delete'])){
									echo "<p>Keyword Delete Failed</p>";
								}
							}

							$rowNumber = 0;	
							foreach (db_select_record("keywords", "userid", $userId) as $temps) {	?>
							<tr>
								<td><?php echo $rowNumber += 1; ?></td>
								<td><?php echo $temps[1]; ?></td>
								<td><?php echo "<a href=".addslashes("?page=config&delete=".$temps[0]).">Delete</a>"; ?></td>
							</tr>
						<?php } ?>
						</tbody>
					</table>
				</div>
				<div class="col-md-4">
					<h4 class="text-center">Site Lists</h4>
					<form class="form-inline" method="POST" action="index.php?page=config">
						<div class="form-group">
						    <input type="text" class="form-control" id="websiteAdd" placeholder="add website.." name="websiteAdd">
						    <button type="submit" class="btn btn-default" name="submitWebsite">Add Site</button>
						</div>
					</form>
					<br />
					<table class="table table-bordered table-hover table-condensed">
						<thead>
							<tr>
								<th>No</th>
								<th>Website</th>
								<th>Act</th>
							</tr>
						</thead>
						<tbody>
						<?php
							$userId = $_SESSION['userId'];
							if (isset($_POST['submitWebsite'])) {
								$input = $_POST['websiteAdd'];

								if(user_add("websites", $input, $userId)){
									echo "<p>Website Added</p>";
								}else{
									echo "<p>Website Add Error !</p>";
								}
							}else if (isset($_GET['deleteweb'])) {
								if(db_delete_record("websites", "id", $_GET['deleteweb'])){
									echo "<p>Website Deleted</p>";
								}else if(!db_delete_record("websites", "id", $_GET['deleteweb'])){
									echo "<p>Website Delete Failed</p>";
								}
							}
							
							$rowNumber = 0;	
							foreach (db_select_record("websites", "userid", $userId) as $temps) {	?>
							<tr>
								<td><?php echo $rowNumber += 1; ?></td>
								<td><?php echo $temps[1]; ?></td>
								<td><?php echo "<a href=".addslashes("?page=config&deleteweb=".$temps[0]).">Delete</a>"; ?></td>
							</tr>
						<?php } ?>
						</tbody>
					</table>
				</div>
				<div class="col-md-2"></div>

			</div>

<?php
	}else if(!$isUserLogin && isset($_GET['page']) && $_GET['page'] == 'regist'){
?>
			<div class="row"
	>			<div class="col-md-12">
					<h3 class="text-center">
						Register
					</h3>
					<br />
					<br />
				</div>
			</div>

			<div class="row">
				<div class="col-md-3">
				</div>
				<div class="col-md-3">
				<?php				
					if(!isset($_POST['submit'])){
				?>
					<form class="form" method="POST" action="index.php?page=regist">
						<div class="form-group">
							<label for="username">Username</label>
						    <input type="text" class="form-control" id="username" placeholder="example" name="username">
						</div>
						<div class="form-group">
							<label for="password">Password</label>
						    <input type="password" class="form-control" id="password" placeholder="password" name="password">
						</div>
						<div class="form-group">
							<label for="email">Email</label>
						    <input type="email" class="form-control" id="email" placeholder="example@mail.com" name="email">
						</div>
						<button type="submit" class="btn btn-default" name="submit">Sign Up !</button>
					</form>
				<?php 	
						}else if(isset($_POST['submit'])){ 
							if(user_register($_POST['username'],$_POST['password'], $_POST['email'])){
								echo "<p>Register Completed ! You may login to your account now</p>";
							}else{
								echo "<p>There is an error !, please try again ! : ".mysqli_error()."</p>";
							}
						} 
				?>
				</div>
				<div class="col-md-3">
					<br />
					<p>Hello Welcome to register page, feel free to register</p>
				</div>
				<div class="col-md-3">
				</div>
			</div>
			<br />
<?php
	}else if(!$isUserLogin && !isset($_POST['login'])){
?>
			<div class="row">	
				<div class="col-md-4"></div>
				<div class="col-md-4">
					<form class="form-horizontal" method="POST" action="index.php">
						<div class="form-group">
							<label for="username">Username</label>
						    <input type="text" class="form-control" id="username" placeholder="username" name="username">
						</div>
						<div class="form-group">
							<label for="password">Keyword</label>
						    <input type="password" class="form-control" id="password" placeholder="password" name="password">
						</div>
						<button type="submit" class="btn btn-default" name="login">Login</button>
					</form>
				</div>
				<div class="col-md-4"></div>
			</div>
<?php
	}

	//Login and Logout Function !
	if(isset($_POST['login'])){
		if(user_login(addslashes($_POST['username']), addslashes($_POST['password']))){
			echo "<p>Login Success</p>";
		}else{
			echo "<p>Login Failed</p>";
		}
	}else if($isUserLogin && isset($_GET['logout'])){
		user_logout();
		echo "<p>Logout Success</p>";
	}

	require_once("inc/footer.php");
?>