-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 02, 2015 at 08:56 AM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `chkserp`
--

-- --------------------------------------------------------

--
-- Table structure for table `keywords`
--
use chkserp;
CREATE TABLE IF NOT EXISTS `keywords` (
  `id` int(10) NOT NULL,
  `keyword` text NOT NULL,
  `userid` int(5) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `keywords`
--

INSERT INTO `keywords` (`id`, `keyword`, `userid`) VALUES
(8, 'contoh plakat', 1),
(9, 'plakat kayu', 1),
(10, 'plakat resin', 1),
(11, 'plakat laser', 1),
(12, 'plakat akrilik', 1),
(13, 'harga plakat', 1),
(14, 'plakat murah', 1),
(15, 'pusat plakat', 1),
(16, 'desain plakat', 1),
(17, 'plakat fiber', 1),
(19, 'design plakat', 1),
(20, 'plakat wisuda', 1),
(21, 'plakat trophy', 1),
(22, 'contoh plakat ucapan terima kasih', 1),
(23, 'pengertian plakat', 1),
(24, 'plakat acrylic', 1),
(25, 'gambar plakat', 1),
(26, 'plakat penghargaan', 1),
(27, 'cara membuat plakat', 1),
(28, 'plakat ucapan terima kasih', 1),
(29, 'plakat kristal', 1),
(30, 'contoh plakat penghargaan', 1),
(31, 'plakat pernikahan', 1),
(32, 'jual plakat', 1),
(33, 'pesan plakat', 1),
(34, 'model plakat', 1),
(35, 'plakat plakat', 1),
(36, 'buat plakat', 1),
(37, 'bikin plakat', 1),
(38, 'pembuatan plakat', 1),
(39, 'plakat kuningan', 1),
(40, 'plakat akrilik murah', 1),
(41, 'bikin akrilik', 1),
(42, 'trophy plakat', 1),
(43, 'jual plakat kayu', 1),
(44, 'plakat kayu murah', 1),
(45, 'buat plakat akriliik', 1),
(46, 'pembuat plakat', 1),
(50, 'google', 2);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(5) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`) VALUES
(1, 'root', '7b24afc8bc80e548d66c4e7ff72171c5', 'root@localhost.com'),
(2, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin@localhost.com');

-- --------------------------------------------------------

--
-- Table structure for table `websites`
--

CREATE TABLE IF NOT EXISTS `websites` (
  `id` int(11) NOT NULL,
  `website` varchar(255) NOT NULL,
  `userid` int(5) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `websites`
--

INSERT INTO `websites` (`id`, `website`, `userid`) VALUES
(1, '1souvenir.com', 1),
(6, 'contohplakat.com', 1),
(7, 'google.com', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `keywords`
--
ALTER TABLE `keywords`
  ADD PRIMARY KEY (`id`), ADD KEY `userid` (`userid`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `username` (`username`,`email`);

--
-- Indexes for table `websites`
--
ALTER TABLE `websites`
  ADD PRIMARY KEY (`id`), ADD KEY `userid` (`userid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `keywords`
--
ALTER TABLE `keywords`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `websites`
--
ALTER TABLE `websites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `keywords`
--
ALTER TABLE `keywords`
ADD CONSTRAINT `keywords_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `websites`
--
ALTER TABLE `websites`
ADD CONSTRAINT `websites_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
