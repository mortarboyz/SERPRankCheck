<?php
	require_once("inc/func.php");

	session_start();
	$isUserLogin = isset($_SESSION['user']);
?>

<!DOCTYPE html>
<html lang="en">
 	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	    <title>chkserp</title>

	    <!-- Bootstrap -->
	    <link href="css/bootstrap.min.css" rel="stylesheet">

	    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	    <!--[if lt IE 9]>
	      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	    <![endif]-->
  	</head>
	<body>
	 	<div class="container-fluid">
			<div class="row">
				<nav class="navbar navbar-inverse navbar-static-top" role="navigation">
						<div class="navbar-header">							 
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
								 <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
							</button> <a class="navbar-brand" href="index.php">chkserp</a>
						</div>
						
						<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
							<ul class="nav navbar-nav">
								<!-- <li>
									<a href="index.php">Home</a>
								</li> -->

								<?php if(!$isUserLogin){ ?>

								<li>
									<a href="index.php?page=regist">Register</a>
								</li>

								<?php }else if($isUserLogin){ ?>
								<li>
									<a href="index.php?page=config">Config</a>
								</li>
								<li>
									<a href="index.php?logout">Logout</a>
								</li>

								<?php } ?>
							
							</ul>
						</div>
				</nav>
			</div>
			